﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T>
        where T: BaseEntity
    {
        private readonly List<T> _inMemoryEnities;

        public InMemoryRepository(IEnumerable<T> collection)
        {
            _inMemoryEnities = new List<T>(collection);
        }

        public Task DeleteAsync(T entity)
        {
            _inMemoryEnities.RemoveAll(e => e.Id == entity.Id);

            return Task.CompletedTask;
        }

        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(_inMemoryEnities as IEnumerable<T>);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(_inMemoryEnities.Find(e => e.Id == id));
        }

        public Task SaveAsync(T entity)
        {
            _inMemoryEnities.RemoveAll(e => e.Id == entity.Id);
            _inMemoryEnities.Add(entity);

            return Task.CompletedTask;
        }
    }
}