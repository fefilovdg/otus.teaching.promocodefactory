using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    internal class EmployeeModelConverter    
    {
        public Employee ToEployee(EmployeeCreateOrUpdate source)
        {
            return (source == null)
                ? null
                : new Employee { Email = source.Email, FirstName = source.FirstName, LastName = source.LastName };
        }

        public EmployeeShortResponse ToResponse(Employee source)
        {
            return (source == null)
                ? null
                : new EmployeeShortResponse { Email = source.Email, FullName = source.FullName, Id = source.Id };
        }
    }
}