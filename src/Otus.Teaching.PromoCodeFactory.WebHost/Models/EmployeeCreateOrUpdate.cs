namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeCreateOrUpdate
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}